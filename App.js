

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  NativeModules,
  PermissionsAndroid
} from 'react-native';
import SendSMS from 'react-native-sms';

const Temp=NativeModules.DeviceModule;


const requestSendSms = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.SEND_SMS,
      {
        title: "Cool Photo App Camera Permission",
        message:
          "Cool Photo App needs access to your camera " +
          "so you can take awesome pictures.",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
};

const requestRecSms = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.RECEIVE_SMS,
      {
        title: "Cool Photo App Camera Permission",
        message:
          "Cool Photo App needs access to your camera " +
          "so you can take awesome pictures.",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
};

class App extends React.Component{
constructor(props){
  super(props)
  this.state={
    name:"sam"
  }
}

componentDidMount=()=>{
  requestSendSms()
  requestRecSms()
}

fun=async()=>{
  // console.log("sam")
  Temp.sendDirectSms("9665938860","I love You sam............kkkkkkkkkkkkkkkk")
  console.log("sam")

}


 initiateSMS = () => {
  // Check for perfect 10 digit length
  const mobileNumber="9665938860"
  const bodySMS="sam shende"
  // if (mobileNumber.length != 10) {
  //   alert('Please insert correct contact number');
  //   return;
  // }

  SendSMS.send(
    {
      // Message body
      body: bodySMS,
      // Recipients Number
      recipients: [mobileNumber],
      // An array of types 
      // "completed" response when using android
      successTypes: ['sent', 'queued'],
    },
    (completed, cancelled, error) => {
      if (completed) {
        console.log('SMS Sent Completed');
      } else if (cancelled) {
        console.log('SMS Sent Cancelled');
      } else if (error) {
        console.log('Some error occured');
      }
    },
  );
};


render(){
  return(
  <Text onPress={()=>{this.fun()}} style={{textAlign:'center',flex:1}}>{this.state.name}</Text>
  )
}

}

export default App;
