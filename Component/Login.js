
import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    NativeModules,
    PermissionsAndroid,
    Label,
    TextInput,
    Button,
    Pressable,
    Image,
    ToastAndroid
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


const Temp=NativeModules.DeviceModule;

const requestSendSms = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.SEND_SMS,
        {
          title: "sms permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the sms");
      } else {
        console.log("sms permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  };
class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            mobileNumber: '',
            error: false,
            errorlen: false
        }
    }

    componentDidMount=()=>{
        requestSendSms()
    }

    sendOtp = async() => {
      
        if (this.state.mobileNumber == '') {
            this.setState({
                error: true
            })
            return
        }
        else {
            if (this.state.mobileNumber.length != 10) {
                this.setState({
                    errorlen: true
                })
            }
            else{
                // var min = 1111;
                // var max = 9999;
                // var rand = min + (Math.random() * (max - min));
                // var str1=rand.toString()
                // var str = str1.split('.')[0]; 
                // console.log(str)
                Temp.sendDirectSms(
                    this.state.mobileNumber,
                    (msg) => {
                      console.error(msg)
                    }, 
                    (someData) => {
                      console.log(someData)
                      if(someData.message=="Successfully"){
                        ToastAndroid.show("Otp Send Successfully", ToastAndroid.SHORT)
                         this.props.navigation.navigate('VerifyOtp',
                         {
                             data:someData
                         }
                         )
                      }
                      else{
                        ToastAndroid.show("Something went wrong", ToastAndroid.SHORT)

                      }
                    }
                  )
            }
        }
    }

    onChangeText = (text) => {
        // console.log(text)
        this.setState({
            error: false,
            mobileNumber: text,
            errorlen: false
        })
    }


    render() {
        // console.log("login")
        return (
            <View style={{ backgroundColor: 'white', height: '100%' }}>

                <View style={{ height: 60, backgroundColor: 'grey' }}>
                    {/* <Text style={{ position: 'absolute', top: 20, left: 10, fontSize: 20, color: 'grey' }}>✕</Text> */}
                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', fontSize: 18, marginTop: 20, color: 'white' }}>Continue With Phone</Text>
                </View>
                <ScrollView>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    >

                        <Image
                            resizeMode='contain'
                            style={{ height: 250, width: 300, alignSelf: 'center',marginTop:20 }}
                            source={require('./ss.png')}
                        />

                        <Text style={{ textAlign: 'center', width: 250, fontWeight: 'bold', fontSize: 20, color: 'grey', marginBottom: 10,marginTop:12 }}>You'll receive 4 digit code to verify next.</Text>


                        <Text style={{ alignSelf: 'center', fontWeight: 'bold', fontSize: 16, color: 'grey', marginBottom: 10 }}>Enter Your Phone<Text style={{ color: 'red' }}>*</Text></Text>
                        <TextInput
                            value={this.state.mobileNumber}
                            onChangeText={text => this.onChangeText(text)}


                            style={{
                                backgroundColor: 'white',
                                borderRadius: 10, borderWidth: 0.5,
                                borderColor: 'silver', marginTop: 5,
                                width: wp('95%'), alignSelf: 'center'
                            }}
                            keyboardType='numeric'
                        />
                        {this.state.error && this.state.mobileNumber == '' &&
                            <Text style={{ color: 'red', marginTop: 3 }}>
                                Please Enter Mobile Number...{' '}
                            </Text>
                        }
                        {this.state.errorlen && this.state.mobileNumber.length != 10 &&
                            <Text style={{ color: 'red', marginTop: 3 }}>
                                Please Enter 10 Digit Mobile Number...{' '}
                            </Text>
                        }
                        <Pressable onPress={() => this.sendOtp()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', height: 45, width: 150, backgroundColor: 'orange', borderRadius: 10, borderColor: 'white', borderWidth: 2, marginTop: 10 }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>Continue</Text>
                            </View>
                        </Pressable>
                    </View>
                </ScrollView>

            </View>

        )
    }

}

export default Login;