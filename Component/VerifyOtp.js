
import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    NativeModules,
    PermissionsAndroid,
    Pressable,
    ToastAndroid
} from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import SmsRetriever from 'react-native-sms-retriever';



class VerifyOtp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mobileNumber: 'sam',
            running: true,
            OTPCode: '',
        }
    }


componentDidMount=()=>{
    // this._onPhoneNumberPressed()
    // this._onSmsListenerPressed()
}

    _onPhoneNumberPressed = async () => {
        try {
          const phoneNumber = await SmsRetriever.requestPhoneNumber();
          console.log(phoneNumber)
        } catch (error) {
          console.log(JSON.stringify(error));
        }
       };

       _onSmsListenerPressed = async () => {
        try {
          const registered = await SmsRetriever.startSmsRetriever();
          if (registered) {
            SmsRetriever.addSmsListener(event => {
              console.log(event.message);
              SmsRetriever.removeSmsListener();
            }); 
          }
        } catch (error) {
          console.log(JSON.stringify(error));
        }
      };

    VerifyOtpFun=()=>{
        // console.log("in ver",this.state.OTPCode)
        if(this.state.OTPCode==this.props.navigation.state.params.data.Otp){
            ToastAndroid.show("Your Otp is Verify Successfully", ToastAndroid.SHORT)
        this.props.navigation.navigate('CorrectOtp')
        }
        else{
            ToastAndroid.show("Your Otp is Incorrect", ToastAndroid.SHORT)
        }
    }

    render() {
    // console.log(this.props.navigation.state.params.data.Otp)
        let str="<"
        return (
            <View style={{ backgroundColor: 'white', height: '100%' }}>

                <View style={{ height: 60, backgroundColor: 'grey' }}>
        <Text onPress={()=>{this.props.navigation.navigate('Login')}} style={{ position: 'absolute', top:0, left: 10, fontSize: 40, color: 'white' }}>{str}</Text>
                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', fontSize: 18, marginTop: 20, color: 'white' }}>Verify Phone</Text>
                </View>
                <ScrollView>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    >
                        <Text style={{ textAlign: 'center', width: 250, fontWeight: '900', fontSize: 20, color: 'grey', marginBottom: 10,marginTop:20 }}>Code is send to {this.props.navigation.state.params.data.mobileNo}</Text>

                        <OTPInputView
                            style={{ width: '60%', height: 40, marginTop: 50 }}
                            codeInputHighlightStyle={{ borderBottomColor: 'white', borderBottomWidth: 2 }}
                            codeInputFieldStyle={{ color: 'grey' }}
                            placeholderCharacter="*"
                            onCodeChanged={OTPCode => { this.setState({ OTPCode }) }}
                            placeholderTextColor="lightgray"
                            pinCount={4} />

                        <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'white', marginTop: 11 }}>00 :</Text>
                        <Pressable onPress={() => this.VerifyOtpFun()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', height: 45, width:wp('90%'), backgroundColor: 'orange', borderRadius: 10, borderColor: 'white', borderWidth: 2, marginTop: 10 }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>Verify and Create Account</Text>
                            </View>
                        </Pressable>
                    </View>
                </ScrollView>

            </View>

        )
    }

}



export default VerifyOtp;
