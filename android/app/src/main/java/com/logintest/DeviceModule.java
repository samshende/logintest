package com.logintest;
import android.widget.Toast;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;
import java.lang.Math;

import android.telephony.SmsManager;

public class DeviceModule extends ReactContextBaseJavaModule {
    //constructor
    private static  ReactApplicationContext reactContext;

    public DeviceModule(ReactApplicationContext context) {
        super(context);
        reactContext=context;
    }
    //Mandatory function getName that specifies the module name
    
    @Override
    public String getName() {
        return "DeviceModule";
    }
    //Custom function that we are going to export to JS
    @ReactMethod
    public void sendDirectSms(String phoneNumber,Callback errorCallback, Callback successCallback) {
        System.out.println("sendDirectSms"+phoneNumber);

        int min = 1000;
        int max = 9999;
        System.out.println("Random value of type int between "+min+" to "+max+ ":");
        int b = (int)(Math.random()*(max-min+1)+min);
        String msg="Your otp is"+" "+b;
        System.out.println("sendDirectSms"+msg);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(
                    phoneNumber, null, msg, null, null
            );
            WritableMap params = Arguments.createMap();
            params.putString("mobileNo", phoneNumber);
            params.putInt("Otp", b);
            params.putString("message", "Successfully");
            successCallback.invoke(params);

        } catch (Exception ex) {
            System.out.println("couldn't send message.");
            errorCallback.invoke(ex.getMessage());
        }

    }

    @ReactMethod
    public void getDeviceName(String message,int Duration) {


//        Sms ss=new Sms();
//        String mob="9665938860";
//        String sms="opt";
//        ss.sendSMS(mob,sms);
        Toast.makeText(getReactApplicationContext(),message,Duration).show();
    }
}