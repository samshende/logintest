package com.logintest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Sms extends Activity
{
    //...

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        //...
        super.onCreate(savedInstanceState);
    }

    //---sends an SMS message to another device---
    public void sendSMS(String phoneNumber, String message)
    {
        System.out.println("in sms fun");
        Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
        smsIntent.setData(Uri.parse(phoneNumber));
        smsIntent.putExtra("sms_body", message);
        System.out.println("in sms fun"+smsIntent);
//            startActivity(smsIntent);

    }
}
