
import React from 'react';
import {createStackNavigator} from 'react-navigation'

import Login from './Component/Login'
import VerifyOtp from './Component/VerifyOtp'
import CorrectOtp from './Component/CorrectOtp'
// import {
//     Root
//   } from 'native-base';

const Navigator=createStackNavigator(

    {
        Login:{
            screen:Login,navigationOptions:{
                header:null
            },
        },
        VerifyOtp:{
            screen:VerifyOtp,navigationOptions:{
                header:null
            },
        },
        CorrectOtp:{
            screen:CorrectOtp,navigationOptions:{
                header:null
            },
        },
    },

    {
        initialRouteName:'Login',
        headerMode:'none',
        mode:'Model'
    }
)


const App=(props)=>(
    // <Root>
    <>

        <Navigator/>
        </>
    // </Root>
)

export default App;